def reverser
  yield.split.map(&:reverse).join(" ")
end

def adder(num_to_add = 1)
  yield + num_to_add
end

def repeater(times_to_repeat = 1)
  (1..times_to_repeat).each { yield }
end

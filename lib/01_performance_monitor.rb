require 'time'

def measure(times = 1)
  earlier_time = Time.now
  (1..times).each { yield }
  later_time = Time.now
  (later_time - earlier_time) / times
end
